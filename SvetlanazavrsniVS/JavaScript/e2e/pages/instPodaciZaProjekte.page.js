// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var InstPodaciZaProjekte = function() {};
var utils = require('../helper/utils.js');

// Opis polja i metoda
InstPodaciZaProjekte.prototype = Object.create({}, {

  // broj racuna

  brojRacuna: {
      get: function() {
          return utils.waitForElementPresence(by.xpath("//input[@name='account']"), 10000);
      },
      set: function(value) {
       this.brojRacuna.clear();
       this.brojRacuna.sendKeys(value);
      }
  },

      

  // identifikacioni broj u ministarstvu

  identBrojUMinistarstvu: {
      get: function() {
          return utils.waitForElementPresence(by.xpath("//input[@name='mntrID']"), 10000);
      },
      set: function(value) {
       this.identBrojUMinistarstvu.clear();
       this.identBrojUMinistarstvu.sendKeys(value);
      }
  },

// identifikacioni broj na medjunarodnom nivou

  identBrojMedjunarodni: {
      get: function() {
          return utils.waitForElementPresence(by.xpath("//input[@name='orcid']"), 10000);
      },
      set: function(value) {
       this.identBrojMedjunarodni.clear();
       this.identBrojMedjunarodni.sendKeys(value);
      }
  },

statusInstitucije: {
    get: function() {
        return utils.waitForElementPresence(by.name("institutionStatus"), 10000);
    },
    set: function(value) {
            this.statusInstitucije.element(by.cssContainingText('option', value)).click();

    }
},

// oblast istrazivanja

oblastIstrazivanja: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//input[@class='select2-input select2-default']"), 10000);
    },
    set: function(value) {
     this.oblastIstrazivanja.clear();
     this.oblastIstrazivanja.sendKeys(value);
    }
},

// Error poruke o grešci

brojRacunaError: {
    get : function() {
        return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti broj računa.']"), 10000);
    }
},


brojRacunaFormatError: {
    get : function() {
        return utils.waitForElementPresence(by.xpath("//span[text()='Broj računa mora biti u formatu XXX-XXXXXXXXXXXXX-XX.']"), 10000);
    }
},


statusInstitucijeError: {
    get : function() {
        return utils.waitForElementPresence(by.xpath("//span[text()='Morate izabrati status institucije.']"), 10000);
    }
},


// Dugmad //button[@name='btnSave']/i[@class='fa fa-check']/following-sibling::[text()='Sačuvaj']

sacuvajBtn: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//form[@name='Project']//button//text()[contains(.,' Sačuvaj')]/.."), 10000);
    }
},

odustaniBtn: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//form[@name='Project']//button//text()[contains(.,' Odustani')]/.."), 10000);
    }
},

//Unos cele forme
 unosSvihOsnovnihPodataka: {
 value: function(brojRacuna, identBrojUMinistarstvu, identBrojMedjunarodni, statusInstitucije, oblastIstrazivanja ) {
        this.brojRacuna = brojRacuna;
        this.identBrojUMinistarstvu = identBrojUMinistarstvu;
        this.identBrojMedjunarodni = identBrojMedjunarodni;
        this.statusInstitucije = statusInstitucije;
        this.oblastIstrazivanja = oblastIstrazivanja;

        this.sacuvajBtn.click();
    }
},
//*****************************************************************
});

module.exports = InstPodaciZaProjekte;
