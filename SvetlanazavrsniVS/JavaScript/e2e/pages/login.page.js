// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var LoginPage = function() {};
var utils = require('../helper/utils.js');

// Opis polja i metoda
LoginPage.prototype = Object.create({}, {

    // Username polje
    username: {
        get: function() {
            return utils.waitForElementPresence(by.id("username"), 10000);
        },
        set: function(value) {
            this.username.clear();
            this.username.sendKeys(value);
        }
    },

    usernameError: {
      get: function() {
        // return utils.waitForElementPresence(by.xpath("//span[text()='Korisničko ime obavezno']"), 10000);
        return utils.waitForElementPresence(by.xpath("//span[text()='Korisničko ime obavezno']")).getText();
      }
    },


    // Password polje
    password: {
        get: function() {
            return utils.waitForElementPresence(by.id("password"), 10000);
        },
        set: function(value) {
            this.password.clear();
            this.password.sendKeys(value);
        }
    },

    passwordError: {
      get: function() {
    //    return utils.waitForElementPresence(by.xpath("//span[text()='Lozinka obavezna']"), 10000);
    return element(by.xpath("//span[text()='Lozinka obavezna']")).getText();
      }
    },
     wrongUsernameAndPassword:{
        get: function() {
            return utils.waitForElementPresence(by.xpath("//li[text()='Pogrešno korisničko ime ili lozinka!']")).getText();
        } 
     },

    // Sign in button
    prijaviSeBtn: {
        get: function() {
            return utils.waitForElementPresence(by.className("btn-info"), 10000);
        }
    },

    odustaniBtn: {
      get: function() {
        return utils.waitForElementPresence(by.className("btn-danger"), 10000);
      }
    },
//getgreskaKodPrijave()
    greskaKodPrijave: {
       get: function() {
         return element(by.repeater('error in errors track by $index')).getText();
  //       return element(by.xpath("//h4//span/following-sibling::text()")).getText();
       }
    },

    zaboravljenaLozinkaBtn: {
       get: function() {
           return element(by.buttonText('Zaboravili ste lozinku?'));
       }
    },


    // Metoda za prelazak na ovu stranicu
    navigateToPage: {
        value: function() {
            browser.get("http://localhost:8080/#/login");
            browser.wait(function() {
                return browser.getCurrentUrl().then(function(url) {
                    return url === '"http://localhost:8080/#/login"';
                });
            }, 5000)
        }
    },

    // Metoda za login
    login: {
        value: function(usernameString, passwordString) {
            this.username = usernameString;
            this.password = passwordString;
            this.prijaviSeBtn.click();
        }
    }
});

// Export klase
module.exports = LoginPage;
