// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var MenuPage = function() {};
var utils = require('../helper/utils.js');

// Opis polja i metoda
MenuPage.prototype = Object.create({}, {

  //zastavica
  zastavica: {
      get: function() {
          return element(by.xpath("//a[@class='dropdown-toggle']"));
      }
  },

odjavaZastava: {
get: function() {
return element(by.css('#main-page > header > nav > div > ul:nth-child(2) > li > a > img'));
}

},
  odjava: {
      get: function() {
          return element(by.xpath("//span[@translate='LOGOUT']"));
      }
  },
  //********************************************************************

  //sidebar

  sidebar: {
      get: function() {
          return element(by.className('sidebar-toggle'));
      }
  },

  //menu-bar sa leve strane
  institucijaIcon: {
      get: function() {
          return element(by.xpath("//a[@ui-sref='adminInstitution']"));
      }
  },

  istrazivaciIcon: {
      get: function() {
          return element(by.xpath("//a[@ui-sref='persons']"));
      }
  },


  //Podmeni za menjanje jezika
  jezikBtn: {
      get: function() {
          return element(by.className('caret'));
      }
  },

   srpskiLatBtn: {
      get: function() {
          return element(by.linkText("Srpski"));
      }
  },

  srpskiCirBtn: {
      get: function() {
          return element(by.linkText("Српски"));
      }
  },

  engleskiBtn: {
      get: function() {
          return element(by.linkText("English"));
      }
  },
  //****************************************************************

  osnovniPodaciBtn: {
      get: function() {
        return element(by.xpath("//a/tab-heading[text()='Osnovni podaci']/.."));
    }
  },

 /* podaciZaRegistarBtn: {
      get: function() {
        return element(by.xpath("//a/tab-heading[text()='Podaci za registar']/.."));
      }
  },*/

podaciZaRegistarBtn: {
      get: function() {
        return utils.waitToBeClickable(by.xpath("//a/tab-heading[text()='Podaci za registar']/.."));
      }
  },
  

  podaciZaProjekteBtn: {
    get: function() {
      return utils.waitToBeClickable(by.xpath("//a/tab-heading[text()='Podaci za projekte']/.."));
    }
  },

  istrazivaciBtn: {
    get: function() {
      return element(by.xpath("//a/tab-heading[text()='Istraživači']/.."));
    }
  },

  dodajIstrazivacaBtn: {
    get: function() {
      return element(by.xpath("//a[@ui-sref='addPerson']"))
    }
  },

angazujOsobuBtn: {
  get: function() {
    return element(by.xpath("//button[@class='btn btn-lg btn-info pull-right ng-binding']"))
  }
},


});

// Export klase
module.exports = MenuPage;
