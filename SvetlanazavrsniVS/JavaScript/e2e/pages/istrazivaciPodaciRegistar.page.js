// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var IstrazivaciPodaciRegistar = function() {};
var utils = require('../utils.js');

// Opis polja i metoda
IstrazivaciPodaciRegistar.prototype = Object.create({}, {

// bibliografija

bibliografija: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//input[@name='bibliography']"), 10000);
    },
    set: function(value) {
     this.bibliografija.clear();
     this.bibliografija.sendKeys(value);
    }
},

// oblasti istrazivanja

oblastiIstrazivanja: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//input[@name='researchAreas']"), 10000);
    },
    set: function(value) {
     this.oblastiIstrazivanja.clear();
     this.oblastiIstrazivanja.sendKeys(value);
    }
},

// identifikacioni broj u ministarstvu

identBrojUMinistarstvu: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//input[@name='mntrn']"), 10000);
    },
    set: function(value) {
     this.identBrojUMinistarstvu.clear();
     this.identBrojUMinistarstvu.sendKeys(value);
    }
},

// napomena

napomena: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//input[@name='note']"), 10000);
    },
    set: function(value) {
     this.napomena.clear();
     this.napomena.sendKeys(value);
    }
},



//Dugmad

sacuvajBtn: {
    get: function() {
        return utils.waitForElementPresence(by.buttonText("Sačuvaj"), 10000);
    }
},

odustaniBtn: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//form[@name='Register']//button//text()[contains(.,' Odustani')]/.."), 10000);
    }
},

//Unos cele forme
 unosSvihOsnovnihPodataka: {
 value: function(bibliografija, oblastiIstrazivanja, identBrojUMinistarstvu, napomena) {
        this.bibliografija = bibliografija;
        this.oblastiIstrazivanja = oblastiIstrazivanja;
        this.identBrojUMinistarstvu = identBrojUMinistarstvu;
        this.napomena = napomena;


        this.sacuvajBtn.click();
    }
},
//*****************************************************************
});

module.exports = IstrazivaciPodaciRegistar;
