// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var InstOsnovniPodaci = function() {};
var utils = require('../helper/utils.js');

// Opis polja i metoda
InstOsnovniPodaci.prototype = Object.create({}, {
nazivModel: {
      get: function() {
          return utils.waitForElementPresence(by.model("data.name"), 10000);
      },
      set: function(value) {
       this.nazivModel.clear();
       this.nazivModel.sendKeys(value);
      }
  },
  

  naziv: {
      get: function() {
          return utils.waitForElementPresence(by.xpath("//input[@name='name']"), 10000);
      },
      set: function(value) {
       this.naziv.clear();
       this.naziv.sendKeys(value);
      }
  },

  nazivEng: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='eng_name']"), 10000);
    },
    set: function(value) {
      this.nazivEng.clear();
      this.nazivEng.sendKeys(value);
    }
  },
//drop down izaberi state koja vec postoji, a kad je dodaj novu otvara nam se pilja za naziv i opis
  drzava: {
      get: function() {
          return utils.waitForElementPresence(by.xpath("//div[@class='col-sm-6 col-print-6']//select[@name='state']/.."), 10000);
      },
      set: function(value) {
       this.drzava.element(by.cssContainingText('option', value)).click();
      }
  },
  

  //ovo je kad dodjemo na novu državu koje nema u listi
  //U funkciju "drzava" treba da prosledimo value "Dodaj novu..."
  // i onda se otvara opcija, naziv,OpisNoveDrzave,NAZIV

  novaDrzava: {
  get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='stateName']"), 10000);
  },
  set: function(value) {
   this.novaDrzava.clear();
   this.novaDrzava.sendKeys(value);
  }
},

  //Opis Nove Drzave
  opisNoveDrzave: {
  get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='stateDescription']"), 10000);
  },
  set: function(value) {
   this.opisNoveDrzave.clear();
   this.opisNoveDrzave.sendKeys(value);
  }
},
//dugme otkaceno
  potvrdiNovuDrzavu: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//div[@class='col-sm-3 text-right']/button"), 10000);
    }
  },
//iz jednog poteza
  unesiNovuDrzavu: {
    value: function(novaDrzava, opisNoveDrzave) {
           this.novaDrzava = novaDrzava;
           this.opisNoveDrzave = opisNoveDrzave;

           this.potvrdiNovuDrzavu.click();
      }
  },
  ////////////////////////////ovo je za ispis teksta drzave by model

  drzavaByModel: {
  get: function() {
      return utils.waitForElementPresence(by.model("data.state"), 10000);
  },
  set: function(value) {
   this.drzavaByModel.clear();
   this.drzavaByModel.sendKeys(value);
  }
},
  //mesto

mesto: {
  get: function() {
    return utils.waitForElementPresence(by.xpath("//input[@name='place']"), 10000);
  },
  set: function(value) {
    this.mesto.clear();
    this.mesto.sendKeys(value);
  }
},

//opstina

opstina: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//input[@name='townShipText']"), 10000);
    },
    set: function(value) {
     this.opstina.clear();
     this.opstina.sendKeys(value);
    }
},

ulica: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//input[@name='address']"), 10000);
    },
    set: function(value) {
     this.ulica.clear();
     this.ulica.sendKeys(value);
    }
},

web: {
   get: function() {
    return utils.waitForElementPresence(by.xpath("//input[@name='uri']"), 10000);
   },
   set: function(value) {
    this.web.clear();
    this.web.sendKeys(value);
   }
},

mail: {
   get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='email']"), 10000);
   },
   set: function(value) {
    this.mail.clear();
    this.mail.sendKeys(value);
   }
},

telefon: {
   get: function() {
       return utils.waitForElementPresence(by.xpath("//input[@name='phone']"), 10000);
   },
   set: function(value) {
    this.telefon.clear();
    this.telefon.sendKeys(value);
   }
},

skraceniNaziv: {
   get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='acro']"), 10000);
   },
   set: function(value) {
    this.skraceniNaziv.clear();
    this.skraceniNaziv.sendKeys(value);
   }
},

//Error poruke o grešci

 nazivInstitucijeError: {
    get : function() {
        return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti naziv.']"), 10000);
    }
},

mestoError: {
    get : function() {
      return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti mesto.']"), 10000);
    }
},

ulicaError: {
    get : function() {
        return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti adresu.']"), 10000);
    }
},

webError: {
    get : function() {
        return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti veb adresu.']"), 10000);
      }
},

emailError: {
    get : function() {
        return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti adresu elektronske pošte.']"), 10000);
    }
},

telefonError: {
    get : function() {
        return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti broj telefona.']"), 10000);
    }
},

//Dugmici menjano
sacuvajBtn: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//form[@name='Basic']//button//text()[contains(.,' Sačuvaj')]/.."), 10000);
    }
},


odustaniBtn: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//form[@name='Basic']//button//text()[contains(.,' Odustani')]/.."), 10000);
    }
},

// DropDown

institucijaDDklik: {
    get: function() {
        return element(by.xpath("//a[@class='select2-choice']"));
    }
},

institucijaDropDown: {
        get: function() {
            return utils.waitForElementPresence(by.className('select2-choice'), 10000);
        },
        set: function() {
            this.institucijaDropDown.element(by.xpath("//span[text()='Fakultet tehničkih nauka']")).click();
        }
    },
    institucijaDropDownVinca: {
        get: function() {
            return utils.waitForElementPresence(by.className('select2-choice'), 10000);
        },
        set: function() {
            this.institucijaDropDownVinca.element(by.id("select2-result-label-39")).click();
        }
    },


//Cela forma Clear all
  formaClearAll: {
     value: function() {
             this.naziv = "";
             this.nazivEng = "";
             this.drzava = "Srbija";
             this.gradEng = "";
             this.opstina = "";
             this.ulica = "";
             this.web = "";
             this.mail = "";
             this.telefon = "";
             this.skraceniNaziv = "";

        }
},

unosObaveznihPodataka: {
  value: function(naziv, nazivEng, drzava, mesto, ulica, web, mail, telefon) {
    this.naziv = naziv;
    this.nazivEng = nazivEng;
    this.drzava = drzava;
    this.mesto = mesto;
    this.ulica = ulica;
    this.web = web;
    this.mail = mail;
    this.telefon = telefon;

    this.sacuvajBtn.click();
  }
},

//Unos cele forme
 unosSvihOsnovnihPodataka: {
 value: function(naziv, nazivEng, drzava, mesto, ulica, web, mail, telefon) {
        this.naziv = naziv;
        this.nazivEng = nazivEng;
        this.drzava = drzava;
        this.mesto = mesto;
      //this.opstina = opstina;
        this.ulica = ulica;
        this.web = web;
        this.mail = mail;
        this.telefon = telefon;
      //this.skraceniNaziv = skraceniNaziv;

        //this.sacuvajBtn.click();
    }
}

//*****************************************************************
});

module.exports = InstOsnovniPodaci;
   
    /*ispitniRok: {
        get: function() {
            return utils.waitForElementPresence(by.id('field_ispitniRok'), 10000);
        },
        set: function(value) {
            this.ispitniRok.element(by.cssContainingText('option', value)).click();
        }
    },*/


/*
StudentCreationPage.prototype = Object.create({}, {
    
    modalDialog: {
        get: function() {
            return utils.waitForElementPresence(by.className('modal-dialog'), 10000);
        }
    }*/
   /* expect(studentsCreationPage.modalDialog.isDisplayed()).toBe(true);
expect(studentsCreationPage.modalDialogTitle.getText()).toEqual("Create or edit a Studenti");*/