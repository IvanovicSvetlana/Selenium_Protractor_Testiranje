// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var InstPodaciZaRegistar = function() {};
var utils = require('../helper/utils.js');

// Opis polja i metoda
InstPodaciZaRegistar.prototype = Object.create({}, {

  //pib

  pib: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='pib']"), 10000);
    },
    set: function(value) {
      this.pib.clear();
      this.pib.sendKeys(value);
    }
  },


  pibError: {
      get : function() {
          return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti PIB.']"), 10000);
      }
  },

  pibFormatError: {
      get : function() {
          return utils.waitForElementPresence(by.xpath("//span[text()='Poreski broj nije u dobrom formatu.']"), 10000);
      }
  },



  //maticniBroj

  maticniBroj: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='maticniBroj']"), 10000);
    },
    set: function(value) {
      this.maticniBroj.clear();
      this.maticniBroj.sendKeys(value);
    }
  },

  maticniBrojError: {
      get : function() {
          return utils.waitForElementPresence(by.xpath("//span[text()='Unesite matični broj.']"), 10000);
      }
  },

  maticniBrojFormatError: {
      get : function() {
          return utils.waitForElementPresence(by.xpath("//span[text()='Matični broj se mora sastojati od 13 cifara']"), 10000);
      }
  },


//brojPoslednjeAkred

  brojPoslednjeAkred: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='accreditationNumber']"), 10000);
    },
    set: function(value) {
      this.brojPoslednjeAkred.clear();
      this.brojPoslednjeAkred.sendKeys(value);
    }
  },

  brojPoslednjeAkredError: {
      get : function() {
          return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti broj akreditacije.']"), 10000);
      }
  },

  //datumPoslednjeAkred

  datumPoslednjeAkred: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//em-date-time-picker[@name='accreditationDate']/span/div/input"), 10000);
    },
    set: function(value) {
      this.datumPoslednjeAkred.clear();
      this.datumPoslednjeAkred.sendKeys(value);
    }
  },

  datumPoslednjeAkredError: {
      get : function() {
          return utils.waitForElementPresence(by.xpath("//span[text()='Unesite datum akreditacije!']"), 10000);
      }
  },


// naziv institucije za akreditacije

  nazivInstIzAkred: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='accreditationNote']"), 10000);
    },
    set: function(value) {
      this.nazivInstIzAkred.clear();
      this.nazivInstIzAkred.sendKeys(value);
    }
  },

  nazivInstIzAkredError: {
      get : function() {
          return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti naziv institucije iz akreditacije.']"), 10000);
      }
  },

// napomena o registru

  napomenaORegistru: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='note']"), 10000);
    },
    set: function(value) {
      this.napomenaORegistru.clear();
      this.napomenaORegistru.sendKeys(value);
    }
  },

// status institucije podaci za Registar
vrstaInstitucijeRegistar: {
    get: function() {
        return utils.waitForElementPresence(by.name("institutionType"), 10000);
    },
    set: function(value) {
            this.vrstaInstitucijeRegistar.element(by.cssContainingText('option', value)).click();

    }
},
vrstaInstitucijeAllOption: {
    get: function() {
        return element.all(by.css(".ng pristine option"));
    }
    },

  vrstaInstitucijeError: {
      get : function() {
          return utils.waitForElementPresence(by.xpath("//span[text()='Morate izabrati vrstu institucije.']"), 10000);
      }
  },

// osnovna delatnost institucije

/*osnovnaDelatnostInst: {
  get: function() {
    return utils.waitForElementPresence(by.id("s2id_autogen18"), 10000);
  },*/
 

// osnivac

osnivac: {
  get: function() {
    return utils.waitForElementPresence(by.name("founder"), 10000);
  },
  set: function(value) {
    this.osnivac.clear();
    this.osnivac.sendKeys(value);
  }
},

// datumOsnivanja

datumOsnivanja: {
  get: function() {
    return utils.waitForElementPresence(by.xpath("//em-date-time-picker[@name='date']/span/div/input"), 10000);
  },
  set: function(value) {
    this.datumOsnivanja.clear();
    this.datumOsnivanja.sendKeys(value);
  }
},

// broj resenja o osnivanju

brojResenjaOOsnivanju: {
  get: function() {
    return utils.waitForElementPresence(by.name("rescriptNumber"), 10000);
  },
  set: function(value) {
    this.brojResenjaOOsnivanju.clear();
    this.brojResenjaOOsnivanju.sendKeys(value);
  }
},

// vlasnicka struktura

vlasnickaStruktura: {
  get: function() {
    return utils.waitForElementPresence(by.name("ownershipStructure"), 10000);
  },
  set: function(value) {
            this.vlasnickaStruktura.element(by.cssContainingText('option', value)).click();

    }
},
    naziv: {
        get: function() {
            return utils.waitForElementPresence(by.name('ownershipStructure'), 10000);
        },
        set: function(value) {
            this.naziv.clear();
            this.naziv.sendKeys(value);
        }
    },
   /* nastavnici: {
        get: function() {
            return utils.waitForElementPresence(by.name('nastavnici'), 10000);
        },
        set: function(nastavniciLabels) {
            var nastavnici = this.nastavnici;
            _.each(nastavniciLabels, function(nastavnikLabel) {
                nastavnici.element(by.cssContainingText('option', nastavnikLabel)).click();
            });
        }
    },
     createPredmet: {
        value: function(naziv, studenti, nastavnici) {
            this.naziv = naziv;
            this.studenti = studenti;
            this.nastavnici = nastavnici;
            
            this.saveBtn.click();
        }
    }
});

    */
    vlasnickaStruktura: {
    get : function() {
        return utils.waitForElementPresence(by.name("ownershipStructure"), 10000);
    }
},
    
vlasnickaStrukturaError: {
    get : function() {
        return utils.waitForElementPresence(by.xpath("//span[text()='Morate izabrati vlasničku strukturu.']"), 10000);
    }
},

// Dugmici

sacuvajBtn: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//form[@name='Register']//button//text()[contains(.,' Sačuvaj')]/.."), 10000);
    }
},

odustaniBtn: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//form[@name='Register']//button//text()[contains(.,' Odustani')]/.."), 10000);
    }
},

//Unos cele forme
 unosSvihOsnovnihPodataka: {
 value: function(pib, maticniBroj, brojPoslednjeAkred, datumPoslednjeAkred, nazivInstIzAkred, napomenaORegistru, vrstaInstitucije, osnovnaDelatnostInst, osnivac, datumOsnivanja, brojResenjaOOsnivanju, vlasnickaStruktura) {
        this.pib = pib;
        this.maticniBroj = maticniBroj;
        this.brojPoslednjeAkred = brojPoslednjeAkred;
        this.datumPoslednjeAkred = datumPoslednjeAkred;
        this.nazivInstIzAkred = nazivInstIzAkred;
        this.napomenaORegistru = napomenaORegistru;
        this.vrstaInstitucije = vrstaInstitucije;
        this.osnovnaDelatnostInst = osnovnaDelatnostInst;
        this.osnivac = osnivac;
        this.datumOsnivanja = datumOsnivanja;
        this.brojResenjaOOsnivanju = brojResenjaOOsnivanju;
        this.vlasnickaStruktura = vlasnickaStruktura;

        this.sacuvajBtn.click();
    }
}



});

module.exports = InstPodaciZaRegistar;
