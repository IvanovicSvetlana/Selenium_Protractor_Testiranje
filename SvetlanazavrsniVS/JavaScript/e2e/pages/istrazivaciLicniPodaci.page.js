// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var IstrazivaciLicniPodaci = function() {};
var utils = require('../helper/utils.js');

// Opis polja i metoda
IstrazivaciLicniPodaci.prototype = Object.create( {}, {

  // ime

  ime: {
      get: function() {
          return utils.waitForElementPresence(by.xpath("//input[@name='firstNameText']"), 10000);
      },
      set: function(value) {
       this.ime.clear();
       this.ime.sendKeys(value);
      }
  },
   imeByModel: {
      get: function() {
          return utils.waitForElementPresence(by.model("data.firstName"), 10000);
      },
      set: function(value) {
       this.imeByModel.clear();
       this.imeByModel.sendKeys(value);
      }
  },


  // prezime

  prezime: {
      get: function() {
          return utils.waitForElementPresence(by.xpath("//div[@name='personSearchLastNameT']/input"), 10000);
      },
      set: function(value) {
       this.prezime.clear();
       this.prezime.sendKeys(value);
      }
  },

  // ime jednog roditelja

  imeJednogRoditelja: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='middleName']"), 10000);
    },
    set: function(value) {
      this.imeJednogRoditelja.clear();
      this.imeJednogRoditelja.sendKeys(value);
    }
  },

// titula Istrazivaca

 titulaIstrazivaca: {
        get: function() {
            return utils.waitForElementPresence(by.name('personTitle'), 10000);
        },
        set: function(value) {
            this.titulaIstrazivaca.element(by.cssContainingText('option', value)).click();
        }
    },

  // datum rodjenja

    datumRodjenja: {
      get: function() {
        return utils.waitForElementPresence(by.xpath("//em-date-time-picker[@name='dateOfBirth']/span/div/input"), 10000);
      },
      set: function(value) {
        this.datumRodjenja.clear();
        this.datumRodjenja.sendKeys(value);
      }
    },

  // drzava rodjenja

    drzavaRodjenja: {
      get: function() {
        return utils.waitForElementPresence(by.xpath("//input[@name='state']"), 10000);
      },
      set: function(value) {
        this.drzavaRodjenja.clear();
        this.drzavaRodjenja.sendKeys(value);
      }
    },

  // mesto rodjenja

  mestoRodjenja: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='placeOfBirth']"), 10000);
    },
    set: function(value) {
      this.mestoRodjenja.clear();
      this.mestoRodjenja.sendKeys(value);
    }
  },

  // opstina rodjenja

  opstinaRodjenja: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='townShipOfBirth']"), 10000);
    },
    set: function(value) {
      this.opstinaRodjenja.clear();
      this.opstinaRodjenja.sendKeys(value);
    }
  },

  // drzava boravista

  drzavaBoravista: {
    get: function() {
      return utils.waitForElementPresence(by.xpath("//input[@name='stateOfResidence']"), 10000);
    },
    set: function(value) {
      this.drzavaBoravista.clear();
      this.drzavaBoravista.sendKeys(value);
    }
  },

// mesto boravista

mestoBoravista: {
  get: function() {
    return utils.waitForElementPresence(by.xpath("//input[@name='city']"), 10000);
  },
  set: function(value) {
    this.mestoBoravista.clear();
    this.mestoBoravista.sendKeys(value);
  }
},

// opstina boravista

opstinaBoravista: {
  get: function() {
    return utils.waitForElementPresence(by.xpath("//input[@name='townShipOfResidence']"), 10000);
  },
  set: function(value) {
    this.opstinaBoravista.clear();
    this.opstinaBoravista.sendKeys(value);
  }
},

// ulica i broj

ulicaIBroj: {
  get: function() {
    return utils.waitForElementPresence(by.xpath("//input[@name='address']"), 10000);
  },
  set: function(value) {
    this.ulicaIBroj.clear();
    this.ulicaIBroj.sendKeys(value);
  }
},

// pol


polSvetlana: {
        get: function() {
            return utils.waitForElementPresence(by.name('gender'), 10000);
        },
        set: function(value) {
            this.polSvetlana.element(by.cssContainingText('option', value)).click();
        }
    },

izabraniPol:{
      get: function() {
          return element(by.css("select[name=gender] option:checked"));
      }
},

// jmbg

jmbg: {
  get: function() {
    return utils.waitForElementPresence(by.xpath("//input[@name='jmbg']"), 10000);
  },
  set: function(value) {
    this.jmbg.clear();
    this.jmbg.sendKeys(value);
  }
},

// elektronska posta

email: {
  get: function() {
    return utils.waitForElementPresence(by.xpath("//input[@name='email']"), 10000);
  },
  set: function(value) {
    this.email.clear();
    this.email.sendKeys(value);
  }
},

// telefon

telefon: {
  get: function() {
    return utils.waitForElementPresence(by.xpath("//input[@name='phones']"), 10000);
  },
  set: function(value) {
    this.telefon.clear();
    this.telefon.sendKeys(value);
  }
},

//licna web adresa

licnaWebAdresa: {
  get: function() {
    return utils.waitForElementPresence(by.xpath("//input[@name='uri']"), 10000);
  },
  set: function(value) {
    this.licnaWebAdresa.clear();
    this.licnaWebAdresa.sendKeys(value);
  }
},

// status istrazivaca
statusIstrazivaca: {
        get: function() {
            return utils.waitForElementPresence(by.name('personStatus'), 10000);
        },
        set: function(value) {
            this.statusIstrazivaca.element(by.cssContainingText('option', value)).click();
        }
    },


//Error poruke o grešci

imeError: {
    get : function() {
  //      return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti ime.']"), 10000);
  return element(by.xpath("//span[text()='Morate uneti ime.']")).getText();
    }
},

jmbgError: {
    get : function() {
  //      return utils.waitForElementPresence(by.xpath("//span[text()='Morate uneti JMBG.']"), 10000);
  return element(by.xpath("//span[text()='Morate uneti JMBG.']")).getText();

    }
},

datumRodjenjaError: {
    get: function() {
      return element(by.xpath("//span[text()='Datum rođenja mora biti u formatu DD.MM.GGGG']")).getText();
    }
},

//Dugmad


sacuvajBtnIstrazivacCeo: {
    get: function() {
        return utils.waitToBeClickable(by.css("#page-content > div > div > div > div > div > div > div > div > div > div.tab-pane.ng-scope.active > ng-include > div > div > div > div > div.panel-body > form.form-horizontal.row-border.ng-pristine.ng-invalid.ng-invalid-required.ng-valid-date.ng-valid-email > div:nth-child(19) > div.col-sm-4 > div > button.btn.btn-lg.btn-success.ng-binding"));
    }
},

sacuvajBtn: {
    get: function() {
        return utils.waitToBeClickable(by.xpath("//*[@id='page-content']/div/div/div/div/div/div/div/div/div/div[1]/ng-include/div/div/div/div/div[2]/form[3]/div[19]/div[2]/div/button[2]"), 10000);
    }
},

odustaniBtn: {
    get: function() {
        return utils.waitForElementPresence(by.xpath("//form[@name='Basic']//button//text()[contains(.,' Odustani')]/.."), 10000);
    }
},

//Unos dela forme

unosDelaOsnovnihPodataka: {
  value: function (ime, prezime, jmbg) {
    this.ime = ime;
    this.prezime = prezime;
    this.jmbg = jmbg;
  }
},

//Unos cele forme
 unosSvihOsnovnihPodataka: {
 value: function(ime, prezime, imeJednogRoditelja, titulaIstrazivaca, datumRodjenja, drzavaRodjenja, mestoRodjenja, opstinaRodjenja, drzavaBoravista, mestoBoravista, opstinaBoravista, ulicaIBroj, polSvetlana, jmbg, email, telefon, licnaWebAdresa, statusIstrazivaca) {
        this.ime = ime;
        this.prezime = prezime;
        this.imeJednogRoditelja = imeJednogRoditelja;
        this.titulaIstrazivaca = titulaIstrazivaca;
        this.datumRodjenja = datumRodjenja;
        this.drzavaRodjenja = drzavaRodjenja;
        this.mestoRodjenja = mestoRodjenja;
        this.opstinaRodjenja = opstinaRodjenja;
        this.drzavaBoravista = drzavaBoravista;
        this.mestoBoravista = mestoBoravista;
        this.opstinaBoravista = opstinaBoravista;
        this.ulicaIBroj = ulicaIBroj;
        this.polSvetlana = polSvetlana;
        this.jmbg = jmbg;
        this.email = email;
        this.telefon = telefon;
        this.licnaWebAdresa = licnaWebAdresa;
        this.statusIstrazivaca = statusIstrazivaca;

        this.sacuvajBtnIstrazivaciCeo.click();
    }
},
//*****************************************************************

});

module.exports = IstrazivaciLicniPodaci;
