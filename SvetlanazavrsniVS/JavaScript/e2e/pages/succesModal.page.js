var utils = require('../helper/utils.js');
var SuccesModalPage = function() {};

SuccesModalPage.prototype = Object.create({}, {

      succesModalAppears: {
        get: function() {
          //return element(by.xpath('//div[@class="ui-pnotify-text"]'));
         // return element(by.xpath("//h4[text()='SUCCESS']/.."));
          return utils.waitForElementPresence(by.className('modal-dialog'), 15000);
        }
    },

    pauseBtn: {
        get: function() {
            return element(by.xpath('//span[@class="fa fa-pause"]'));
        }
    },

    playBtn: {
        get: function() {
            return element(by.xpath('//span[@class="fa fa-play"]'));
        }
    },

    xBtn: {
        get: function() {
            return element(by.xpath('//span[@class="fa fa-times"]'));
        }
    },
    ///////////Proba za title modalnog dijaloga
   // <div class="ui-pnotify-text">Uspešno ste sačuvali podatke o instituciji.</div>
  succesModalTitle: {
        get: function() {
        
          return utils.waitForElementPresence(by.xpath('//div[@class="ui-pnotify-text"]'), 15000);
    }
  }
});

module.exports = SuccesModalPage;
