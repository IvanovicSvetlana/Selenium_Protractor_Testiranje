/*Pre-condition:1>Login page is available...
Assumption :Approach of writing test case is based on UI
testing due to lack of functional requirement..
Positive Test Cases:
1. Enter Valid User Name and Valid Password and click "OK"
2. Check the availabilty of Edit box of user name and
password.
3. Check that OK and Cancel button are in enabled mode.
4. Check that after entering Valid user id and valid
password after clicking on tab button of keyboard cursor
should be highlighted on "Ok" button.
5. Check vertical alighnment of user id and password edit
boxes.
6. Check "Cacel button" functionality..window should be
disappeared.
Negative Test Cases:
1. Enter Valid User Name and InValid Password and click "OK"
2. Enter Invalid User Name and Valid Password and click "OK"
3. Enter Valid User Name and Valid Password and click "OK"
4. Leave user id and password field blank and hit "OK"
button.
5.Clck on the grey area of window and check behavior.

var list = element.all(by.css('.items));

var list2 = element.all(by.repeater('personhome.results'));

var list3 = element.all(by.xpath('//div

expect(list.count()).toBe(3);

expect(list.get(0).getText()).toBe('First’)

expect(list.get(1).getText()).toBe('Second’)

expect(list.first().getText()).toBe('First’)

expect(list.last().getText()).toBe('Last’)

browser.actions().mouseMove(registration.alertNotHidden).perform();

expect(succesModalPage.succesModalTitle.getText()).toMatch('Uspešno ste sačuvali podatke o instituciji.')
//button[contains(text(),'Odustani')]

 angazujBtn: {
      get: function() {
          return utils.waitToBeClickable(by.xpath("//form[@name='Persons']//button//text()[contains(.,' Angažuj')]/.."), 10000);
      }
  },

  odustaniBtn: {
      get: function() {
          return utils.waitForElementPresence(by.xpath("//form[@name='Persons']//button//text()[contains(.,' Odustani')]/.."), 10000);
      }
  },

*/



/*function waitForElementTextToChange (elementId, textToWaitFor) {
    return browser.wait(function () {
        return element(by.id(elementId)).getText().then(function (text) {
                return text === textToWaitFor;
            },
            function () {
                return element(by.id(elementId)).getText().then(function (text) {
                    return text === textToWaitFor;
                });
            }
        );
    });
}*/