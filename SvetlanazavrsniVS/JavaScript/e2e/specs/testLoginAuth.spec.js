//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
 //var MenuPage = require('../pages/menuPage.page.js');

//describe
describe('Test Login page:', function() {
  var loginPage;
  var menuPage;

    //"Pre svega" - navigiramo se na stranicu i instanciramo page-ove
    beforeAll(function() {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');

});

it('should have all the expected page elements.', function() {

    expect(loginPage.username.isDisplayed()).toBe(true);
    expect(loginPage.username.isEnabled()).toBe(true);

    expect(loginPage.password.isDisplayed()).toBe(true);
    expect(loginPage.password.isEnabled()).toBe(true);

    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);
});

it('username is required.', function() {
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);
    loginPage.password = 'aaaaa';
    loginPage.username = 'a';
    loginPage.username.clear();
    var ocekivanaPoruka = "Korisničko ime obavezno";
    expect(loginPage.usernameError).toEqual(ocekivanaPoruka);
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);

});


it('password is required.', function() {
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);
    loginPage.username = 'nesto';
    loginPage.password = 'a';
    loginPage.password.clear();
    var ocekivanaPoruka = "Lozinka obavezna";
    //expectujemo je sa porukom koju smo uhvatili na Page-u sa selector.getText
    expect(loginPage.passwordError).toEqual(ocekivanaPoruka);
    //Prijavi se dugme ne treba da bude Enabled
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);

});

//novi test - sada sa pogrešnim loginom i passwordom
it('Nemogućnost da se uloguje sa pogrešnim userom i passwordom', function() {

    //unosimo pogrešne logIn i password odjednom
    loginPage.login('pera.peric@gmail.com', 'perap');

    //pravimo varijablu sa textom očekivane poruke
    var ocekivanaPoruka = "Pogrešno korisničko ime ili lozinka!";
    //  greskaKodPrijave getText u Page object
    expect(loginPage.greskaKodPrijave).toEqual(ocekivanaPoruka);

});


});
