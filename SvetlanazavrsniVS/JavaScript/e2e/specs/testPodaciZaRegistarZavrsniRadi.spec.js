//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var InstOsnovniPodaci = require('../pages/instOsnovniPodaci.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var PodaciZaRegistarPage = require('../pages/instPodaciZaRegistar.page.js');

//describe
describe('test TAB-a"Podaci za Registar', function () {
  var loginPage;
  var menuPage;
  var instOsnovniPodaci;
  var succesModalPage;
  var podaciZaRegistarPage;


  beforeAll(function () {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    instOsnovniPodaci = new InstOsnovniPodaci();
    succesModalPage = new SuccesModalPage();
    podaciZaRegistarPage = new PodaciZaRegistarPage();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
    //čekanje da se pojavi log-in stranica
    browser.wait(function () {
      return browser.getCurrentUrl().then(function (url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');
    loginPage.login('djura@djuraminis.com', 'adminvinca');


    menuPage.srpskiCirBtn.click();
    menuPage.srpskiLatBtn.click();


  });

  it('Institucija podaci za registar pib error ', function () {

    //klikni na tab register
    menuPage.podaciZaRegistarBtn.click();

    podaciZaRegistarPage.pib = "";
    podaciZaRegistarPage.maticniBroj = "2302983870000";
    podaciZaRegistarPage.brojPoslednjeAkred = "23";
    podaciZaRegistarPage.datumPoslednjeAkred = "11.11.2016";
    podaciZaRegistarPage.nazivInstIzAkred = "Instutucija";
    podaciZaRegistarPage.napomenaORegistru = "Nesto";
    podaciZaRegistarPage.osnivac = "Test Testic";
    podaciZaRegistarPage.brojResenjaOOsnvanju = "235777";
    expect(podaciZaRegistarPage.pibError.isDisplayed()).toBe(true);
  });

  // Unos institucije bez maticnog broja
  it('Institucija podaci za registar maticni broj error ', function () {

    //klikni na tab register
    menuPage.podaciZaRegistarBtn.click();

    podaciZaRegistarPage.pib = "105957640";

    podaciZaRegistarPage.maticniBroj = "";
    podaciZaRegistarPage.brojPoslednjeAkred = "23";
    podaciZaRegistarPage.datumPoslednjeAkred = "11.11.2016";
    podaciZaRegistarPage.nazivInstIzAkred = "Instutucija";
    podaciZaRegistarPage.napomenaORegistru = "Nesto";
    podaciZaRegistarPage.osnivac = "Test Testic";
    podaciZaRegistarPage.brojResenjaOOsnvanju = "235777";
    expect(podaciZaRegistarPage.maticniBrojError.isDisplayed()).toBe(true);
  });


  //Unos institucije bez datuma poslednje akreditacije

  it('Datum poslednje akreditacije nije unet  ', function () {

    //klikni na tab register
    menuPage.podaciZaRegistarBtn.click();

    podaciZaRegistarPage.pib = "105957640";
    podaciZaRegistarPage.maticniBroj = "2302983870000";
    podaciZaRegistarPage.brojPoslednjeAkred = "23";
    podaciZaRegistarPage.datumPoslednjeAkred = "";
    podaciZaRegistarPage.nazivInstIzAkred = "Instutucija";
    podaciZaRegistarPage.napomenaORegistru = "Nesto";
    podaciZaRegistarPage.osnivac = "Test Testic";
    podaciZaRegistarPage.brojResenjaOOsnvanju = "235777";
    expect(podaciZaRegistarPage.datumPoslednjeAkredError.isDisplayed()).toBe(true);
  });

  // Provera formata Pib-a        

  it('shoud have error messages when PIB is wrong', function () {

    menuPage.podaciZaRegistarBtn.click();
    podaciZaRegistarPage.pib = "sssssss";
    expect(podaciZaRegistarPage.pibFormatError.isDisplayed()).toBe(true);
  });

  it('Provera prazno polje maticnog broja', function () {

    // Provera maticnog broja prazno polje

    menuPage.podaciZaRegistarBtn.click();
    podaciZaRegistarPage.maticniBroj = "sssssss";
    podaciZaRegistarPage.maticniBroj.clear();
    expect(podaciZaRegistarPage.maticniBrojError.isDisplayed()).toBe(true);

  });
  
});
