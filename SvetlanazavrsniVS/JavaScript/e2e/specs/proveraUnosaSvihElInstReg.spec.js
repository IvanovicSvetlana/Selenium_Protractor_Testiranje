//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var InstOsnovniPodaci = require('../pages/instOsnovniPodaci.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var PodaciZaRegistarPage = require('../pages/instPodaciZaRegistar.page.js');

//describe
describe('test TAB-a"Podaci za Registar', function () {
  var loginPage;
  var menuPage;
  var instOsnovniPodaci;
  var succesModalPage;
  var podaciZaRegistarPage;


  beforeAll(function () {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    instOsnovniPodaci = new InstOsnovniPodaci();
    succesModalPage = new SuccesModalPage();
    podaciZaRegistarPage = new PodaciZaRegistarPage();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
    //čekanje da se pojavi log-in stranica
    browser.wait(function () {
      return browser.getCurrentUrl().then(function (url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');
    loginPage.login('djura@djuraminis.com', 'adminvinca');


    menuPage.srpskiCirBtn.click();
    menuPage.srpskiLatBtn.click();


  });

 it('Provera unosa svih elemenata  ', function () {

    //klikni na tab register
    menuPage.podaciZaRegistarBtn.click();

    podaciZaRegistarPage.pib = "105957640";
    podaciZaRegistarPage.maticniBroj = "2302983870000";
    podaciZaRegistarPage.brojPoslednjeAkred = "23";
    podaciZaRegistarPage.datumPoslednjeAkred = "12.12.2002";
    podaciZaRegistarPage.nazivInstIzAkred = "Instutucija";
    podaciZaRegistarPage.napomenaORegistru = "Nesto";
    podaciZaRegistarPage.vrstaInstitucijeRegistar = "Univerzitet";
    podaciZaRegistarPage.osnivac = "Test Testic";
    podaciZaRegistarPage.brojResenjaOOsnvanju = "235777";
    podaciZaRegistarPage.vlasnickaStruktura= "Privatna";
    expect(podaciZaRegistarPage.sacuvajBtn.isDisplayed()).toBe(true);
    podaciZaRegistarPage.sacuvajBtn.click();
    
  });
});