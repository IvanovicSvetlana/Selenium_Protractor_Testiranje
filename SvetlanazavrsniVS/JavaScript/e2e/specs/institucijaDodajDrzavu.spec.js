//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var InstOsnovniPodaci = require('../pages/instOsnovniPodaci.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var PodaciZaProjektePage = require('../pages/instPodaciZaProjekte.page.js');

//describe TestInstitucija POdaci za Projekte
describe('test institucija dodaj drzavu:', function () {
  var loginPage;
  var menuPage;
  var instOsnovniPodaci;
  var succesModalPage;
  var podaciZaProjektePage;

  //"Pre svega" - navigiramo se na stranicu i instanciramo page-ove


  beforeAll(function () {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    instOsnovniPodaci = new InstOsnovniPodaci();
    succesModalPage = new SuccesModalPage();
    podaciZaProjektePage = new PodaciZaProjektePage();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
    //čekanje da se pojavi log-in stranica
    browser.wait(function () {
      return browser.getCurrentUrl().then(function (url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');
    loginPage.login('djura@djuraminis.com', 'adminvinca');


    menuPage.srpskiCirBtn.click();
    menuPage.srpskiLatBtn.click();


  });
it('should add country',function(){

instOsnovniPodaci.drzava="Dodaj novu...";
instOsnovniPodaci.unesiNovuDrzavu("Belgija","nesto");
expect(instOsnovniPodaci.drzava.isDisplayed()).toBe(true);

});

it('should select country',function(){

instOsnovniPodaci.drzava="Srbija";
expect(instOsnovniPodaci.drzava.isDisplayed()).toBe(true);
});

//Drop Down list is not multiple
it('should select multiple country',function(){
  instOsnovniPodaci.novaDrzava.click();
    var option = instOsnovniPodaci.novaDrzava;
    
    // Eksplicitno ceka da pojavi element na prozoru.
    browser.wait(function() {
        return option.isDisplayed();
    }, 50000);
    option.click();
//posto lista nije multiple, samo mi menja drzave nasumicno
var prva=instOsnovniPodaci.drzava="Srbija";
var druga=instOsnovniPodaci.drzava="Belgija";
    builder = browser.actions();
 multipleSelect = builder.keyDown(protractor.Key.CONTROL).click(prva).click(druga);
            
    multipleSelect.perform();



});
});