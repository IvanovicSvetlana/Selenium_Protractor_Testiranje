var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');

describe('Test login page', function () {

    var loginPage;
    var menuPage;

    beforeAll(function () {
        browser.navigate().to("http://localhost:8080/#/login");
        loginPage = new LoginPage();
        menuPage = new MenuPage();
        //Proveravamo da li je prikazan pravi url
        //getCurrentUrl() je metoda!!!
        expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');

    });
    it('should not pass if username field is empty', function () {
        loginPage.username = 'svetlana';
        loginPage.username.clear();
        loginPage.password = 'adminvinca';

        expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);
        expect(loginPage.usernameError).toEqual('Korisničko ime obavezno');

    });
    it('should not login if password field is empty', function () {
        loginPage.login('djura@djuraminis.com', '');
        expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);
        var ocekivanaPoruka = "Lozinka obavezna";
        expect(loginPage.passwordError).toEqual(ocekivanaPoruka);


    });

    it('should not login with wrong username and password', function () {
        loginPage.login('TesT', 'aaaaaaa');

        expect(loginPage.wrongUsernameAndPassword).toEqual('Pogrešno korisničko ime ili lozinka!');


    });
    it('username is required.', function () {
        loginPage.password = 'aaaaa';
        loginPage.username = 'a';
        loginPage.username.clear();
        var ocekivanaPoruka = "Korisničko ime obavezno";
        //expectujemo je sa porukom koju smo uhvatili na Page-u sa selector.getText
        expect(loginPage.usernameError).toEqual(ocekivanaPoruka);


    });
    it('should not have upper case letters in it.', function () {
        loginPage.username = 'TesT';
        loginPage.password = 'aaaa';
        loginPage.prijaviSeBtn.click();
        expect(loginPage.wrongUsernameAndPassword).toContain('Pogrešno korisničko ime ili lozinka!');
    });
    it('should not have symbols  in it.', function () {
        loginPage.username = 'TesT%$%$#';
        loginPage.password = 'aaaa';
        loginPage.prijaviSeBtn.click();

        expect(loginPage.wrongUsernameAndPassword).toContain('Pogrešno korisničko ime ili lozinka!');
    });
    it('should logout', function () {
        loginPage.login('djura@djuraminis.com', 'adminvinca');
        menuPage.odjavaZastava.click();
        menuPage.odjava.click();

    });



});