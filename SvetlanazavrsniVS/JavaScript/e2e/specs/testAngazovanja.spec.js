//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var IstrazivaciLicniPodaci = require('../pages/istrazivaciLicniPodaci.page.js');


//describe
describe('Test  za angazovanje istrazivaca', function() {
  var loginPage;
  var menuPage;
  var istrazivaciLicniPodaci;
  var succesModalPage;

    //Predjem na stranicu i instanciramo page-ove
   beforeAll(function() {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    succesModalPage = new SuccesModalPage();
   istrazivaciLicniPodaci = new IstrazivaciLicniPodaci();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
    //čekanje da se pojavi log-in stranica
    browser.wait(function() {
    return browser.getCurrentUrl().then(function(url) {
      return url === 'http://localhost:8080/#/login';
    });
  }, 5000, 'LogIn stranica ne može da se učita');

});

it('Angazuj istrazivaca', function() {

  //unosim login i password
  loginPage.login('djura@djuraminis.com', 'adminvinca');
  browser.wait(function() {
        return browser.getCurrentUrl().then(function(url) {
          return url === 'http://localhost:8080/#/admin-institution/';
        });
      }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');


  //klikni na Tab istraživači
  menuPage.istrazivaciBtn.click();

  //klikni na Angazuj osobu button

  menuPage.angazujOsobuBtn.click();
  browser.wait(function() {
        return browser.getCurrentUrl().then(function(url) {
          return url === 'http://localhost:8080/#/admin-institution/';
        });
      }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');

// provera obeveznog polja ime

istrazivaciLicniPodaci.ime = 'a';
istrazivaciLicniPodaci.ime.clear();
browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/persons/';
      });
    }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');

var ocekivanaPorukaIme = "Morate uneti ime.";

expect(istrazivaciLicniPodaci.imeError).toEqual(ocekivanaPorukaIme);
expect(istrazivaciLicniPodaci.sacuvajBtn.isEnabled()).toBe(true);

// provera ovaveznog polja jmbg

istrazivaciLicniPodaci.jmbg = '12';
istrazivaciLicniPodaci.jmbg.clear();

var ocekivanaPorukaJmbg = "Morate uneti JMBG.";
expect(istrazivaciLicniPodaci.jmbgError).toEqual(ocekivanaPorukaJmbg);

// provera polja za unos datumRodjenja

istrazivaciLicniPodaci.ime = 'Petar';
istrazivaciLicniPodaci.prezime = 'Petrovic';
istrazivaciLicniPodaci.jmbg = '1234';
istrazivaciLicniPodaci.datumRodjenja = '1234'
istrazivaciLicniPodaci.sacuvajBtn.click();
browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/persons/';
      });
    }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');


var ocekivanaPorukaDatum = "Datum rođenja mora biti u formatu DD.MM.GGGG";
expect(istrazivaciLicniPodaci.datumRodjenjaError).toEqual(ocekivanaPorukaDatum);

// provera unosa novog istrazivaca

istrazivaciLicniPodaci.ime.clear();
istrazivaciLicniPodaci.prezime.clear();
istrazivaciLicniPodaci.jmbg.clear();
istrazivaciLicniPodaci.datumRodjenja.clear();

istrazivaciLicniPodaci.ime = 'Kosta';
istrazivaciLicniPodaci.prezime = 'Kostic';
istrazivaciLicniPodaci.jmbg = '1234KostaKostic'
istrazivaciLicniPodaci.datumRodjenja = '13.03.1999'

istrazivaciLicniPodaci.sacuvajBtn.click();
modalSucces = succesModalPage.succesModalAppears;
       expect(modalSucces.isDisplayed()).toBe(true);

});

});
