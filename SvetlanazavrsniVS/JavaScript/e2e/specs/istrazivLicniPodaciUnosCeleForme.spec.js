//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var InstOsnovniPodaci = require('../pages/instOsnovniPodaci.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var PodaciZaProjektePage = require('../pages/instPodaciZaProjekte.page.js');
var IstrazivaciLicniPodaci = require('../pages/istrazivaciLicniPodaci.page.js');

//describe TestInstitucija POdaci za Projekte
describe('test TAB-a"Podaci za projekat:', function () {
  var loginPage;
  var menuPage;
  var instOsnovniPodaci;
  var succesModalPage;
  var podaciZaProjektePage;
  var istrazivaciLicniPodaci;

  //"Pre svega" - navigiramo se na stranicu i instanciramo page-ove
//Radiiiiii ceo istrazivac unet

  beforeAll(function () {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    instOsnovniPodaci = new InstOsnovniPodaci();
    succesModalPage = new SuccesModalPage();
    podaciZaProjektePage = new PodaciZaProjektePage();
    istrazivaciLicniPodaci = new IstrazivaciLicniPodaci();


    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
    //čekanje da se pojavi log-in stranica
    browser.wait(function () {
      return browser.getCurrentUrl().then(function (url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');

    loginPage.login('djura@djuraminis.com', 'adminvinca');
  });
  it('should login ', function(){


    browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/admin-institution/';
      });
    }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');

    expect(browser.getCurrentUrl()).toContain("admin-institution");

    menuPage.srpskiCirBtn.click();
    menuPage.srpskiLatBtn.click();



    //klikni na ikonu istraživači
    menuPage.istrazivaciIcon.click();

    //klikni na dodaj istrazivaca button

    menuPage.dodajIstrazivacaBtn.click();
     browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/persons/';
      });
    }, 10000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');
   
    expect(browser.getCurrentUrl()).toContain("persons");

//expect(istrazivaciLicniPodaci.sacuvajBtnIstrazivacCeo.isDisplayed()).toBe(true);
//expect(istrazivaciLicniPodaci.sacuvajBtnIstrazivacCeo.isEnabled()).toBe(true);

    istrazivaciLicniPodaci.unosDelaOsnovnihPodataka("ime","prezime","1223234323432");

      istrazivaciLicniPodaci.sacuvajBtnIstrazivacCeo.click();

    //expect((istrazivaciLicniPodaci.ime).getAttribute('value')).toMatch("ime");


  /*  istrazivaciLicniPodaci.unosSvihOsnovnihPodataka("IME","ime","Svetislav","nema"
    ,"20.02.1985","nesto","nesto","nesto","nesto","nnn","hhh","ffff","Ženski","1223344445566",
    "test@test.com","26363636","www.ddd.com","");*/

 

    
    });  });





