var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');

describe('Test Login page:', function() {
  var loginPage;

  beforeAll(function() {
  browser.navigate().to("http://localhost:8080/#/login");
  loginPage = new LoginPage();

  //proveravamo da li je prikazan pravi URL
  expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');

});
//prvi test - logovanje sa ispravnim login i passwordom
it('Mogucnost logovanja kao Svetlana', function() {

     /* //čekanje da se pojavi log-in stranica
      browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');*/

    //dugme PrijaviSe ne treba da bude klikabilno, ali je displayovano
    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isPresent()).toBe(true);
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);
    //dugme OdjaviSe je klikabilno i tu je
    expect(loginPage.odustaniBtn.isEnabled()).toBe(true);
    expect(loginPage.odustaniBtn.isPresent()).toBe(true);
    expect(loginPage.odustaniBtn.isDisplayed()).toBe(true);

//unosim logIn i password u jednom potezu
loginPage.login('djura@djuraminis.com', 'adminvinca');

//čekam da se pojavi glavna stranica
browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'localhost:8080/#/admin-institution/';
      });
    }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');

    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/admin-institution/');

});
});
