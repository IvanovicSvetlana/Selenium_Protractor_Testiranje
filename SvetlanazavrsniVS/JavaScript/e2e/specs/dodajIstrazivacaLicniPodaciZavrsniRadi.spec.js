//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var IstrazivaciLicniPodaci = require('../pages/istrazivaciLicniPodaci.page.js');
var RegistrationPage=require('../pages/registration.page.js');
//describe
describe('Test  za dodavanje istrazivaca', function () {
  var loginPage;
  var menuPage;
  var istrazivaciLicniPodaci;
  var succesModalPage;
  var registration;
  //"Pre svega" - navigiramo se na stranicu i instanciramo page-ove
  beforeAll(function () {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    succesModalPage = new SuccesModalPage();
    istrazivaciLicniPodaci = new IstrazivaciLicniPodaci();
    registration = new RegistrationPage();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
    //čekanje da se pojavi log-in stranica
    browser.wait(function () {
      return browser.getCurrentUrl().then(function (url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');
    loginPage.login('djura@djuraminis.com', 'adminvinca');


    menuPage.srpskiCirBtn.click();
    menuPage.srpskiLatBtn.click();


  });

  it('Add istrazivac', function () {

    //klikni na ikonu istraživači


    // provera obaveznog polja ime
    menuPage.istrazivaciIcon.click();
    menuPage.dodajIstrazivacaBtn.click();

    istrazivaciLicniPodaci.ime = 'a';
    istrazivaciLicniPodaci.ime.clear();
    /* browser.wait(function () {
        return browser.getCurrentUrl().then(function (url) {
          return url === 'http://localhost:8080/#/persons/';
        });
      }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');*/

    var ocekivanaPorukaIme = "Morate uneti ime.";

    expect(istrazivaciLicniPodaci.imeError).toEqual(ocekivanaPorukaIme);
    // expect(istrazivaciLicniPodaci.sacuvajBtn.isEnabled()).toBe(true);
  });

  it('Dodaj istrazivaca bez imena', function () {

    menuPage.istrazivaciIcon.click();
    menuPage.dodajIstrazivacaBtn.click();

    istrazivaciLicniPodaci.ime = 'a';
    istrazivaciLicniPodaci.ime.clear();
    var ocekivanaPorukaIme = "Morate uneti ime.";

    expect(istrazivaciLicniPodaci.imeError).toContain(ocekivanaPorukaIme);

  });
  it('Pogresan unos JMBG', function () {

    istrazivaciLicniPodaci.jmbg = '12';
    istrazivaciLicniPodaci.jmbg.clear();

    var ocekivanaPorukaJmbg = "Morate uneti JMBG.";
    expect(istrazivaciLicniPodaci.jmbgError).toEqual(ocekivanaPorukaJmbg);
  });
  it('Pogresan unos datuma rodjenja', function () {

    istrazivaciLicniPodaci.ime = 'Pera';
    istrazivaciLicniPodaci.prezime = 'Peric';
    istrazivaciLicniPodaci.jmbg = '1234';
    istrazivaciLicniPodaci.datumRodjenja = '1234'
    istrazivaciLicniPodaci.sacuvajBtn.click();


    var ocekivanaPorukaDatum = "Datum rođenja mora biti u formatu DD.MM.GGGG";
    expect(istrazivaciLicniPodaci.datumRodjenjaError).toEqual(ocekivanaPorukaDatum);
  });
  
// browser.actions().mouseMove(registration.alertNotHidden).perform();


  it('Dodaj istrazivaca potvrdi da je tu', function () {

    menuPage.istrazivaciIcon.click();
    menuPage.dodajIstrazivacaBtn.click();

    istrazivaciLicniPodaci.ime = 'Kosta';
    

    expect((istrazivaciLicniPodaci.imeByModel).getAttribute('value')).toMatch('Kosta');

  });

});


