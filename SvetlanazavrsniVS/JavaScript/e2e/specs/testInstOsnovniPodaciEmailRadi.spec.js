//requirovanje page-ova koje smo napravili i koje sad 'uvlačimo ovde'
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var IstrazivaciLicniPodaci = require('../pages/istrazivaciLicniPodaci.page.js');
var InstOsnovniPodaci = require('../pages/instOsnovniPodaci.page.js');

//describe
describe('Test  za dodavanje istrazivaca', function () {
    var loginPage;
    var menuPage;
    var istrazivaciLicniPodaci;
    var succesModalPage;
    var instOsnovniPodaci;
    //'Pre svega' - navigiramo se na stranicu i instanciramo page-ove
    beforeAll(function () {
        browser.navigate().to('http://localhost:8080/#/login');
        loginPage = new LoginPage();
        menuPage = new MenuPage();
        succesModalPage = new SuccesModalPage();
        istrazivaciLicniPodaci = new IstrazivaciLicniPodaci();
        instOsnovniPodaci = new InstOsnovniPodaci();

        //proveravamo da li je prikazan pravi URL
        expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
        //čekanje da se pojavi log-in stranica
        browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === 'http://localhost:8080/#/login';
            });
        }, 5000, 'LogIn stranica ne može da se učita');
        loginPage.login('djura@djuraminis.com', 'adminvinca');


        menuPage.srpskiCirBtn.click();
        menuPage.srpskiLatBtn.click();

    });

    it('Provera prazno polje za email', function () {

        //menuPage.institucijaIcon.click();
        instOsnovniPodaci.mail = 'test@gmail.com';

        instOsnovniPodaci.mail.clear();

        var ocekivanaPorukaEmail = "Morate uneti adresu elektronske pošte.";
        expect(instOsnovniPodaci.emailError.getText()).toContain(ocekivanaPorukaEmail);
    });
    it('Provera prazno polje za web adresu', function () {

        instOsnovniPodaci.web = 'www.vinca.com';

        instOsnovniPodaci.web.clear();

        expect(instOsnovniPodaci.webError.isDisplayed()).toBe(true);
    });

    //Test ne prolazi, jer aplikacija ne ispisuje nikakvu poruku [], konstatujem bug
         //IZLAZ - Expected '' to contain 'Morate uneti adresu elektronske pošte.'.

    it('Provera email bez @', function () {

        instOsnovniPodaci.mail = 'test.tes';
        var ocekivanaPorukaEmail = "Morate uneti adresu elektronske pošte.";
        expect(instOsnovniPodaci.emailError.getText()).toContain(ocekivanaPorukaEmail);
    });
    //POZITIVAN TEST ZA INSTITUCIJU email

        it('Provera email sa @ znakom', function () {

       instOsnovniPodaci.mail = 'test@tes';
       // var ocekivanaPorukaEmail = "";
        //dokaz da se greska ne ispisuje
        expect(instOsnovniPodaci.emailError.getAttribute('value')).toBe(null);
    });
 });

