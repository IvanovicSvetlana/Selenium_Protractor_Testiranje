//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var InstOsnovniPodaci = require('../pages/instOsnovniPodaci.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var PodaciZaProjektePage = require('../pages/instPodaciZaProjekte.page.js');

//describe TestInstitucija POdaci za Projekte
describe('test TAB-a"Podaci za projekat:', function () {
  var loginPage;
  var menuPage;
  var instOsnovniPodaci;
  var succesModalPage;
  var podaciZaProjektePage;

  //"Pre svega" - navigiramo se na stranicu i instanciramo page-ove


  beforeAll(function () {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    instOsnovniPodaci = new InstOsnovniPodaci();
    succesModalPage = new SuccesModalPage();
    podaciZaProjektePage = new PodaciZaProjektePage();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
    //čekanje da se pojavi log-in stranica
    browser.wait(function () {
      return browser.getCurrentUrl().then(function (url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');
    loginPage.login('djura@djuraminis.com', 'adminvinca');


    menuPage.srpskiCirBtn.click();
    menuPage.srpskiLatBtn.click();


  });

  it('Institucija podaci za projekat broj racuna', function () {


    // naziv, nazivEng, drzava, mesto, opstina, ulica, web, mail, telefon, skraceniNaziv
    instOsnovniPodaci.unosSvihOsnovnihPodataka("Vinca", "Vincaa", "Srbija", "Beograd", "Beograd"
      , "Vuka Karadzica 89", "www.vinca.com", "vinca@test.com", "+381657282", "V");



    //klik na Tab "PodaciZaProjekat"

    menuPage.podaciZaProjekteBtn.click();

    // provera da li javlja gresku o formatu broja racuna
    podaciZaProjektePage.brojRacuna = "proba";
    expect(podaciZaProjektePage.brojRacunaFormatError.isDisplayed()).toBe(true);


    // provera kada je format broja racuna ispravan
    podaciZaProjektePage.brojRacuna.clear();
    podaciZaProjektePage.brojRacuna = "123-4567891234567-89";
    expect(podaciZaProjektePage.brojRacunaError.isDisplayed()).toBe(false);
    expect(podaciZaProjektePage.brojRacunaFormatError.isDisplayed()).toBe(false);

  });
  it('Provera nekih elemenata institucije podaci za projekat dugme Identifikacioni broj ', function () {

    menuPage.podaciZaProjekteBtn.click();
    //  provera da je disabled
    expect(podaciZaProjektePage.identBrojUMinistarstvu.isEnabled()).toBe(false);

  });
  it('Provera nekih elemenata institucije podaci za projekat dobar unos', function () {
    // menuPage.institucijaIcon.click();

    //klik na Tab "PodaciZaProjekat"

    menuPage.podaciZaProjekteBtn.click();
    podaciZaProjektePage.brojRacuna = '221-2226363334636-11';
    podaciZaProjektePage.identBrojMedjunarodni = '2322';
    podaciZaProjektePage.statusInstitucije = "U izgradnji";
    //podaciZaProjektePage.oblastIstrazivanja
    //podaciZaProjektePage.sacuvajBtn.click();


    //ovo dole radi
    expect((podaciZaProjektePage.brojRacuna).getAttribute('value')).toEqual('221-2226363334636-11');
  });

  it('should find an element by text input model', function () {
    menuPage.institucijaIcon.click();

    menuPage.podaciZaProjekteBtn.click();
    /* var racun = element(by.name('account'));
     racun.clear();
     racun.sendKeys('221-2226363334636-11');*/

    podaciZaProjektePage.brojRacuna = '221-2226363334636-11';
    //expect(element(by.name('account')).getAttribute('value')).toMatch('221-2226363334636-11');

    expect((podaciZaProjektePage.brojRacuna).getAttribute('value')).toMatch('221-2226363334636-11');
  });
});







