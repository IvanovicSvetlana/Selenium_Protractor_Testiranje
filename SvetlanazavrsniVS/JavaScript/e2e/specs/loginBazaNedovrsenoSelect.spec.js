//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var InstOsnovniPodaci = require('../pages/instOsnovniPodaci.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var PodaciZaProjektePage = require('../pages/instPodaciZaProjekte.page.js');

//describe TestInstitucija POdaci za Projekte
describe('test institucija dodaj drzavu:', function () {
  var loginPage;
  var menuPage;
  var instOsnovniPodaci;
  var succesModalPage;
  var podaciZaProjektePage;

  //"Pre svega" - navigiramo se na stranicu i instanciramo page-ove


  beforeAll(function () {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    instOsnovniPodaci = new InstOsnovniPodaci();
    succesModalPage = new SuccesModalPage();
    podaciZaProjektePage = new PodaciZaProjektePage();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
    //čekanje da se pojavi log-in stranica
    browser.wait(function () {
      return browser.getCurrentUrl().then(function (url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');

    loginPage.login('djura@djuraminis.com', 'adminvinca');


    });
  it('should login as djura@djuraminis.com',function(){
  browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/admin-institution/';
      });
    }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');

    expect(browser.getCurrentUrl()).toContain("admin-institution");

    menuPage.srpskiCirBtn.click();
    menuPage.srpskiLatBtn.click();
    instOsnovniPodaci.naziv = 'Vinca';
    instOsnovniPodaci.nazivEng = 'neki';
    instOsnovniPodaci.drzava = 'Srbija';
    instOsnovniPodaci.mesto = 'Beograd';
    instOsnovniPodaci.opstina = 'Beograd';
    instOsnovniPodaci.skraceniNaziv = 'V';
    instOsnovniPodaci.ulica = 'Vuka Karadzica 51';
    instOsnovniPodaci.web = 'www.vinca.rs';
    instOsnovniPodaci.mail = 'test@gmail.com';
    instOsnovniPodaci.telefon = '012345678';
    instOsnovniPodaci.institucijaDropDown;
   expect(instOsnovniPodaci.sacuvajBtn.isDisplayed()).toBe(true);

    instOsnovniPodaci.sacuvajBtn.click();

     
    menuPage.odjavaZastava.click();
       menuPage.odjava.click();

});


   
   });