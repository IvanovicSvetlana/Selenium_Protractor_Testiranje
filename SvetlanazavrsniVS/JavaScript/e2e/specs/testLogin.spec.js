//requirovanje page-ova koje smo napravili i koje sad "uvlačimo ovde"
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
 //var MenuPage = require('../pages/menuPage.page.js');

//describe
describe('Test Login page:', function() {
  var loginPage;
  var menuPage;

    //"Pre svega" - navigiramo se na stranicu i instanciramo page-ove
    beforeAll(function() {
    browser.navigate().to("http://localhost:8080/#/login");
    loginPage = new LoginPage();
    menuPage = new MenuPage();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');

});

//prvi test - logovanje sa ispravnim login i passwordom
it('Mogucnost logovanja kao djura', function() {

      //čekanje da se pojavi log-in stranica
      browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');

    //dugme PrijaviSe ne treba da bude klikabilno, ali je displayovano
    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isPresent()).toBe(true);
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);
    //dugme OdjaviSe je klikabilno i tu je
    expect(loginPage.odustaniBtn.isEnabled()).toBe(true);
    expect(loginPage.odustaniBtn.isPresent()).toBe(true);
    expect(loginPage.odustaniBtn.isDisplayed()).toBe(true);

//unosim logIn i password u jednom potezu
loginPage.login('djura@djuraminis.com', 'adminvinca');


//čekam da se pojavi glavna stranica
browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/admin-institution/';
      });
    }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');

    expect(browser.getCurrentUrl()).toContain("admin-institution");

//klikni na ikonu istraživači
menuPage.istrazivaciIcon.click();


browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/persons';
      });
    }, 5000, 'Nakon prijave ne uspeva da se prebaci na glavnu stranicu');

//klik opet na ikonu institucije(persons)
menuPage.institucijaIcon.click();

//promena jezika
menuPage.jezikBtn.click();
menuPage.engleskiBtn.click();


//log out
menuPage.zastavica.click();
menuPage.odjava.click();

//čekanje da se pojavi log-in stranica (opet nakon log-outa)
browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'Nakon odjave ne uspeva da se vrati na LogIn stranicu');

});

//novi test - sada sa pogrešnim loginom i passwordom
it('Nemogućnost da se uloguje sa pogrešnim userom i passwordom', function() {

    //unosimo pogrešan username i password odjednom
    loginPage.login('svetlana@gmail.com', 'svetlana');

    //pravimo varijablu sa textom očekivane poruke
    var ocekivanaPoruka = "Pogrešno korisničko ime ili lozinka!";
    //expectujemo je sa porukom koju smo uhvatili na Page-u sa selector.getText
    expect(loginPage.greskaKodPrijave).toEqual(ocekivanaPoruka);

});

});
