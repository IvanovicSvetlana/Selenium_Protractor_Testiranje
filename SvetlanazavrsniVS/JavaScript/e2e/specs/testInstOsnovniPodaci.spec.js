//requirovanje page-ova koje smo napravili i koje sad 'uvlačimo ovde'
var LoginPage = require('../pages/login.page.js');
var MenuPage = require('../pages/menuPage.page.js');
var SuccesModalPage = require('../pages/succesModal.page.js');
var IstrazivaciLicniPodaci = require('../pages/istrazivaciLicniPodaci.page.js');
var InstOsnovniPodaci = require('../pages/instOsnovniPodaci.page.js');

//describe
describe('Test  za dodavanje institucije', function () {
  var loginPage;
  var menuPage;
  var istrazivaciLicniPodaci;
  var succesModalPage;
  var instOsnovniPodaci;
  //'Pre svega' - navigiramo se na stranicu i instanciramo page-ove
  beforeAll(function () {
    browser.navigate().to('http://localhost:8080/#/login');
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    succesModalPage = new SuccesModalPage();
    istrazivaciLicniPodaci = new IstrazivaciLicniPodaci();
    instOsnovniPodaci = new InstOsnovniPodaci();

    //proveravamo da li je prikazan pravi URL
    expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
    //čekanje da se pojavi log-in stranica
    browser.wait(function () {
      return browser.getCurrentUrl().then(function (url) {
        return url === 'http://localhost:8080/#/login';
      });
    }, 5000, 'LogIn stranica ne može da se učita');
  });
      beforeEach(function () {

    loginPage.login('djura@djuraminis.com', 'adminvinca');


    menuPage.srpskiCirBtn.click();
    menuPage.srpskiLatBtn.click();

  });

//test prolazi
  it('Provera obaveznih polja i Mogucnost registrovanja samo sa obaveznim poljima', function () {

    instOsnovniPodaci.naziv = 'Vinca';
    instOsnovniPodaci.nazivEng = 'neki';
    instOsnovniPodaci.drzava = 'Srbija';
    instOsnovniPodaci.mesto = 'Beograd';
    instOsnovniPodaci.opstina = 'Beograd';
    instOsnovniPodaci.skraceniNaziv = 'V';
    instOsnovniPodaci.ulica = 'Vuka Karadzica 51';
    instOsnovniPodaci.web = 'www.vinca.rs';
    instOsnovniPodaci.mail = 'test@gmail.com';
    instOsnovniPodaci.telefon = '012345678';
   expect(instOsnovniPodaci.sacuvajBtn.isDisplayed()).toBe(true);

    instOsnovniPodaci.sacuvajBtn.click();

expect(succesModalPage.succesModalTitle.getText().isDisplayed()).toBe(true);
//************************************/

  });
 

afterAll(function() {
       menuPage.odjavaZastava.click();
       menuPage.odjava.click();
    });

  });
  //button[contains(text(),'Odustani')]
