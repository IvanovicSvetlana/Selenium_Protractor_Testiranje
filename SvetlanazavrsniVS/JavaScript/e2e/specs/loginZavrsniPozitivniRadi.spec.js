var MenuPage = require('../pages/menuPage.page.js');
var RegistrationPage= require('../pages/registration.page.js');
var LoginPage = require('../pages/login.page.js');

describe('Test login page', function () {

    var loginPage;
    var menuPage;
    var registrationPage;

function getNCharacters(n){
    var output = '';
    for (var i = 0; i < n; i++){
        output += 'a';
    }
    return output;
}
   
    beforeAll(function () {
        browser.navigate().to("http://localhost:8080/#/login");
        loginPage=new LoginPage();
        registrationPage = new RegistrationPage();
        menuPage = new MenuPage();
        //Proveravamo da li je prikazan pravi url
        //getCurrentUrl() je metoda!!!
        expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');
        loginPage.username.clear();
        registrationPage.password.clear();

    });
      describe('positive cases should verify that', function() {
        describe('username', function() {

   it('can have 50 characters.', function() {
                // Utvrdimo da nema gresaka
                expect(registrationPage.usernameErrors).toEqual([]);
                loginPage.username = getNCharacters(50);
                expect(registrationPage.usernameErrors).toEqual([]);
            });

            it('can have 1 character.', function() {
                expect(registrationPage.usernameErrors).toEqual([]);
                loginPage.username = getNCharacters(1);
                expect(registrationPage.usernameErrors).toEqual([]);
            });

            it('can have digits in it.', function() {
                expect(registrationPage.usernameErrors).toEqual([]);
                loginPage.username = 'test123';
                expect(registrationPage.usernameErrors).toEqual([]);
            });
 
});
    afterAll(function () {
        // Reloadujemo stranicu nakon izvrsenih testova za username kako bismo
        // dobili cistu stranicu za sledeci test.
        browser.refresh();
    });

});
});