var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var SpecReporter = require('jasmine-spec-reporter');

exports.config = {
    seleniumServerJar: 'node_modules/protractor/selenium/selenium-server-standalone-2.47.1.jar',
    chromeDriver: 'node_modules/protractor/selenium/chromedriver',
    allScriptsTimeout: 60000,

    specs: [
        //'e2e/specs/institucijaDodajDrzavu.spec.js',
        //'e2e/specs/test.spec.js',
          'e2e/specs/loginBazaNedovrsenoSelect.spec.js',
        //'e2e/specs/dodajIstrazivacaLicniPodaciZavrsniRadi.spec.js',
        //'e2e/specs/loginzavrsniRadi.spec.js',
        //'e2e/specs/loginZavrsniPozitivniRadi.spec.js',
        //'e2e/specs/institucPodacizaProjekteBrojRacuna.spec.js',
        //'e2e/specs/proveraUnosaSvihElInstReg.spec.js',
        //'e2e/specs/testLogin.spec.js',
        //'e2e/specs/testLoginAuth.spec.js',
        //'e2e/specs/testPodaciZaProjekteZavrsniRadi.spec.js',
        //'e2e/specs/testAngazovanja.spec.js',
        //'e2e/specs/testInstOsnovniPodaciEmailRadi.spec.js',
        //'e2e/specs/testInstOsnovniPodaci.spec.js',
        //'e2e/specs/testOsnovniPodaci.spec.js',
         //'e2e/specs/testPodaciZaRegistarZavrsniRadi.spec.js',
        // 'e2e/specs/testDodajIstrazivacaOrig.spec.js'


    ],

    capabilities: {
        'browserName': 'chrome',
    },

    directConnect: true,

    baseUrl: 'http://localhost:8080/#/login',

    framework: 'jasmine2',

    jasmineNodeOpts: {
        showColors: true,
        isVerbose: true,
        defaultTimeoutInterval: 30000,
        print: function () { }
    },

    onPrepare: function () {
        // Postavljamo prozor na fullscreen
        browser.driver.manage().window().maximize();

        // Implicitno cekanje 1 sekunda
        browser.manage().timeouts().implicitlyWait(1000);

        // Registrujemo reportere
        jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
            savePath: "./target/reports/e2e/",
            takeScreenshots: true,
            takeScreenshotsOnlyOnFailures: true,
            fixedScreenshotName: true
        }));
        jasmine.getEnv().addReporter(new SpecReporter({
            displayStacktrace: 'all',
            displaySpecDuration: true,
            displayFailuresSummary: false,
            displayPendingSummary: false,
        }));
    }
};
