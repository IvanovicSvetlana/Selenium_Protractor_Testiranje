var gulp = require('gulp');
var protractor = require('gulp-protractor').protractor;

gulp.task('protractor', function() {
    return gulp.src([])
        .pipe(protractor({
            configFile: "protractor.conf.js"
        }));
});
