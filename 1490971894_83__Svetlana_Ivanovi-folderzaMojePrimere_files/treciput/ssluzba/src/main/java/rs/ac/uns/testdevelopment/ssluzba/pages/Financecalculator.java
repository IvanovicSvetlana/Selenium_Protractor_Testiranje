package rs.ac.uns.testdevelopment.ssluzba.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Financecalculator {

	public WebDriver driver;

	public Financecalculator(WebDriver driver) {
		this.driver = driver;
	}
	//<a href="/finance-calculator.html">Finance <img src="c.gif"></a>
	public WebElement getLinkFinanceCalculator() {
		return driver.findElement(By.xpath("//A[@href='/finance-calculator.html']"));
	}
	public WebElement getInputFieldNumberOfPeriods() {
		return driver.findElement(By.id("cyearsv"));
	}
	public void setInputFieldNumberOfPeriods(String value) {
		WebElement input=getInputFieldNumberOfPeriods();
		input.clear();
		input.sendKeys(value);
		
	}
	public WebElement getStartPrincipal() {
		return driver.findElement(By.id("cstartingprinciplev"));
	}
//	<input type="text" name="cstartingprinciplev" id="cstartingprinciplev" value="20000" class="innormal indollar">
	public void setStartPrincipal(String start) {
		WebElement startPrincipal=getStartPrincipal();
		startPrincipal.clear();
		startPrincipal.sendKeys(start);
		}
//	<input type="text" name="cinterestratev" id="cinterestratev" value="6" class="innormal inpct">
	public WebElement getInterest() {
		return driver.findElement(By.xpath("//*[@id='cinterestratev']"));
	}
	public void setInterest(String interest) {
		WebElement kamata=getInterest();
		kamata.clear();
		kamata.sendKeys(interest);
	}
	//<input type="text" name="ccontributeamountv" id="ccontributeamountv" value="1000" class="innormal indollar">
	public WebElement getAnnutyPayment() {
		return driver.findElement(By.id("ccontributeamountv"));
		//table[@id="ccontributeamount"]//descendant::input[@name='ccontributeamountv']
	}
	public void setAnnutyPayment(String value) {
		WebElement placanja=getAnnutyPayment();
		placanja.clear();
		placanja.sendKeys(value);
	}
	//<input name="ciadditionat1" id="ciadditionat1" value="beginning" type="radio">
	public WebElement getCalculateButton1() {
		return (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.elementToBeClickable(By.id("ciadditionat1")));
	}
	//*[@id="cperiod"]/tbody/tr[2]/td/input[2]
	public WebElement getCalculateButtonToBeClicked() {
		return driver.findElement(By.xpath("//*[@id='cperiod']/tbody/tr[2]/td/input[2]"));
	}
	
	public String getResult() {
		return driver.findElement(By.xpath("//*[@id='content']/div[5]/h2")).getText().toUpperCase();
	}
	public boolean isPresent(){
		return driver.findElement(By.xpath("//h1[text()='Finance Calculator']")).getText().equalsIgnoreCase("finance Calculator");
		//za neku drugu stranu pozovem drugi objekat i napravim pre toga novu metodu sa izmenjenim xpath.

	}
	public boolean isPresent1(String value){
		return driver.findElement(By.xpath("//h1[text()='%s']")).getText().equalsIgnoreCase(value);
		//za neku drugu stranu pozovem drugi objekat i napravim pre toga novu metodu sa izmenjenim xpath.
	}
	public WebElement getChildFromParent() {
		return driver.findElement(By.id("cendamount")).findElement(By.className("innormal"));
}
	public boolean isEmptyInputField(){
		return driver.findElement(By.id("cyearsv")).getText().isEmpty();
	}
	}

