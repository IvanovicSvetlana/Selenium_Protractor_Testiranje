package rs.ac.uns.testdevelopment.ssluzba.pages;

import static org.testng.AssertJUnit.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ModalDeletePage {
	private WebDriver driver;
	
	public ModalDeletePage(WebDriver driver){
		this.driver = driver;
	}
	
	public WebElement getModal(){
		return driver.findElement(By.className("modal-dialog"));
	}
	public void confirmDelete() {
		getModal().findElement(By.className("btn-danger")).click();
	}
	/*public WebElement getSignUp() {
		WebElement signUp = (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a [@ui-sref=\"login\"]")));
		return signUp;
	}*/
	public void cancelDelete(){
		getModal().findElement(By.className("btn-default")).click();
	}
}
