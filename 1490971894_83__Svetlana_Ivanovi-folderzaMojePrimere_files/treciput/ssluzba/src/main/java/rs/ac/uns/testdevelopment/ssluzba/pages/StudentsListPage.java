package rs.ac.uns.testdevelopment.ssluzba.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StudentsListPage {
	private WebDriver driver;

	public StudentsListPage(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public WebElement getCreateBtn(){
		WebElement btn = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button [@ui-sref=\"studenti.new\"]")));
		return btn;
//		return driver.findElement(By.xpath("//button [@ui-sref=\"studenti.new\"]"));
	}

	public WebElement getStudentsTable() {
		return driver.findElement(By.className("jh-table"));
	}

	public List<WebElement> getTableRows() {
		return this.getStudentsTable().findElements(By.tagName("tr"));
	}
//posto je indeks sigurno razlicit
	public WebElement getStudentRowByIndex(String index) {
		return driver.findElement(By.xpath("//*[contains(text(),\"" + index + "\")]/../.."));
	}

	public void deleteStudentByIndex(String index) {
		getStudentRowByIndex(index).findElement(By.className("btn-danger")).click();
	}

	public void editStudentByIndex(String index) {
		getStudentRowByIndex(index).findElement(By.className("btn-primary")).click();
	}

	public void viewStudentByIndex(String index) {
		getStudentRowByIndex(index).findElement(By.className("btn-info")).click();
	}

}
