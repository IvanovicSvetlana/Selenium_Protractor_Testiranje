package primeri18;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Primer2 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new FirefoxDriver();
		CalculatorFractionPage fractionPage = new CalculatorFractionPage(driver);
		// Puts an Implicit wait, Will wait for 10 seconds
		// before throwing exception
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Launch website
		driver.navigate().to("http://www.calculator.net/fraction-calculator.html");
		driver.manage().window().maximize();
		// Selecting an item from Drop Down list Box
		fractionPage.setTopElement1("1");
		fractionPage.setBottomElement1("10");
		
		fractionPage.setTopElement2("2");
		fractionPage.setBottomElement2("10");
		
		fractionPage.setOperation("*");
		
		fractionPage.getCalculateButton().click();
		Thread.sleep(3000);
		
		driver.close();
	}
}