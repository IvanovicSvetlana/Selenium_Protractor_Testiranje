package primeri18;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CalculatorFractionPage {
	private WebDriver driver;

	public CalculatorFractionPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getTopElement1(){
		return driver.findElement(By.id("t1"));
	}
	public void setTopElement1(String value){
		WebElement el = this.getTopElement1();
		el.clear();
		el.sendKeys(value);
	}
	public WebElement getTopElement2(){
		return driver.findElement(By.id("t2"));
	}
	public void setTopElement2(String value){
		WebElement el = this.getTopElement2();
		el.clear();
		el.sendKeys(value);
	}
	
	public WebElement getBottomElement1(){
		return driver.findElement(By.id("b1"));
	}
	public void setBottomElement1(String value){
		WebElement el = this.getBottomElement1();
		el.clear();
		el.sendKeys(value);
	}
	public WebElement getBottomElement2(){
		return driver.findElement(By.id("b2"));
	}
	
	public void setBottomElement2(String value){
		WebElement el = this.getBottomElement2();
		el.clear();
		el.sendKeys(value);
	}
	
	public void setOperation(String operation){
		Select dropdown = new Select(driver.findElement(By.id("op")));
		dropdown.selectByValue(operation);
	}
	
	public WebElement getCalculateButton(){
		return driver.findElement(By.xpath(".//*[@id=\"content\"]/table[1]/tbody/tr[4]/td/input[2]"));
	}
	
	
	
}
