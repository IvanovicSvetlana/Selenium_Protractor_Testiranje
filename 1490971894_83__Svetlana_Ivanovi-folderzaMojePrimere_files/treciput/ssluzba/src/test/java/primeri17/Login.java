package primeri17;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Login {
	public static void main(String[] args) throws InterruptedException {
		WebDriver browser = new FirefoxDriver();
		// Puts an Implicit wait, Will wait for 10 seconds
		// before throwing exception
		// instanciranje browsera
		// implicitno vreme cekanja za pronalazenje elemenata
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// maximizuje prozor
		browser.manage().window().maximize();
		// navigacija na zeljenu stranicu
		browser.navigate().to("localhost:9000/#/");
		WebElement accountMenu = browser.findElement(By.id("account-menu"));
		accountMenu.click();
		WebElement signIn = browser.findElement(By.xpath("//a [@ui-sref=\"login\"]"));
		System.out.println(signIn.isDisplayed());

		signIn.click(); // go to login page
		WebElement username = browser.findElement(By.id("username"));
		WebElement password = browser.findElement(By.id("password"));
		WebElement btnSignIn = browser.findElement(By.xpath("//button[@translate='login.form.button']"));

		// check elements visibilityo
		System.out.println(username.isDisplayed());
		System.out.println(password.isDisplayed());
		System.out.println(btnSignIn.isDisplayed());

		// set username value
		username.clear(); // delete current value
		username.sendKeys("admin"); // send new value

		// set password value
		password.clear();
		password.sendKeys("admin");

		// click signin button
		btnSignIn.click();

		// get login message
		String message = browser.findElement(By.xpath("//div [@translate=\"main.logged.message\"]")).getText();
		String expectedMessage = "You are logged in as user \"admin\".";
		// check login message
		System.out.println(expectedMessage);
		// Shutdown the browser
		browser.quit();
		// Close the Browser.

	}
}
