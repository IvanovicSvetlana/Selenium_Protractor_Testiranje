package primeri17;

import org.testng.annotations.Test;

import junit.framework.Assert;
import primeri18.CalculatorPercentagePage;
import primeri19.PrimerchromeMoje;
import rs.ac.uns.testdevelopment.ssluzba.pages.Financecalculator;

import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;

public class FinacialCalculatorTest {
	WebDriver driver;
	private Financecalculator financialCalculator;
	@Test
	public void finacialTest() {
		financialCalculator.getLinkFinanceCalculator().click();
		financialCalculator.isEmptyInputField();
		financialCalculator.setInputFieldNumberOfPeriods("12");
		financialCalculator.setStartPrincipal("50000");
		financialCalculator.setAnnutyPayment("2000");
		financialCalculator.setInterest("10");
		financialCalculator.getCalculateButtonToBeClicked().click();
		financialCalculator.getCalculateButton1().click();
		Assert.assertEquals("RESULTS", financialCalculator.getResult());
Assert.assertTrue(financialCalculator.isPresent());
System.out.println(financialCalculator.isEmptyInputField());
System.out.println(financialCalculator.getChildFromParent());
	}
	@BeforeMethod
	public void setUpSelenium() {
		driver = new FirefoxDriver();
		financialCalculator= new Financecalculator(driver);
		// Puts an Implicit wait, Will wait for 10 seconds
		// before throwing exception
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		// Launch website
		driver.navigate().to("http://www.calculator.net");
		// Maximize the browser
		driver.manage().window().maximize();		
	}
	@AfterMethod
	public void afterMethod() {
		driver.close();
	}

}
