package primeri19;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PrimerValidacijaFormi {
	public static String getNCharacters(int n) {
		StringBuffer outputBuffer = new StringBuffer(n);
		for (int i = 0; i < n; i++) {
			outputBuffer.append("a");
		}
		return outputBuffer.toString();
	}

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "chromedriver");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.get("http://formvalidation.io/examples/complex-form/");
		WebDriver frame = (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("demo-frame")));
		Thread.sleep(1000);
		WebElement form = (new WebDriverWait(frame, 20))
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("movieForm")));
		System.out.println(form.isDisplayed());

		FormPage formPage = new FormPage(frame);

		// TODO 1 validate form elements visibility
		WebElement validateBtn = formPage.getValidateBtn();
		validateBtn.click();
		
		System.out.println(formPage.getTitleRequiredError().getText());
		System.out.println(formPage.getTitleLengthError().isDisplayed());
		System.out.println(validateBtn.isEnabled());

		formPage.setMovieTitle(getNCharacters(201));

		System.out.println(formPage.getTitleRequiredError().isDisplayed());
		System.out.println(formPage.getTitleLengthError().getText());
		System.out.println(validateBtn.isEnabled());

		// TODO 2 validate all fields error messages
		driver.close();
	}
}
