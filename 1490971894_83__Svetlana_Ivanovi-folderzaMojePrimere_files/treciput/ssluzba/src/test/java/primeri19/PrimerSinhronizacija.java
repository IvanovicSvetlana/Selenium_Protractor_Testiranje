package primeri19;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class PrimerSinhronizacija {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://formvalidation.io/examples/complex-form/");
		WebDriver frame = (new WebDriverWait(driver,10))
				.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("demo-frame")));
		WebElement form = (new WebDriverWait(frame,10))
				.until(ExpectedConditions.presenceOfElementLocated(
				By.id("movieForm")));
		System.out.println(form.isDisplayed());

	/*	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
			.withTimeout(60, TimeUnit.SECONDS)
			.pollingEvery(10, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class);
		WebElement dynamicelement = wait.until(new Function<WebDriver,WebElement>()
		{
			public WebElement apply(WebDriver driver)
			{
			return driver.findElement(By.id("movieForm"));
		}
		});
		System.out.println(frame.findElement(By.id("movieForm")).isDisplayed());
	*/
		//System.out.println(dynamicelement.isDisplayed());
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.close();
	}

}
