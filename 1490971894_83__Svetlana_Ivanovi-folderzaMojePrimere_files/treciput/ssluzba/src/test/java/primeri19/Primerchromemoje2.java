package primeri19;

import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;

public class Primerchromemoje2 {
	private WebDriver driver;
	private PrimerchromeMoje primerchrome;

	@Test
	public void testiranjealculatora() {
		primerchrome.setAge("31");
		primerchrome.setHeight("174");
		primerchrome.getFemaleToBeClicked().click();
		primerchrome.setWeight("87");
		primerchrome.getOptionExtraActive().click();//radi kad sam stavila id
		primerchrome.getCalculateButton().click();
		Assert.assertTrue(primerchrome.getTitleResult().isDisplayed());
	}

	@BeforeMethod
	public void setUpSelenium() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		driver.navigate().to("http://www.calculator.net/carbohydrate-calculator.html");
		// instanciranje objekata
		System.out.println(driver.getTitle());//to je onaj title unutar taga
		primerchrome = new PrimerchromeMoje(driver);
	}
}
