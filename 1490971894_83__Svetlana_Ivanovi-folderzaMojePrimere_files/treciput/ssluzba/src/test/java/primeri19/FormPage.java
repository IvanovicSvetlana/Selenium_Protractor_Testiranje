package primeri19;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FormPage {
	WebDriver driver;

	public FormPage(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public WebElement getValidateBtn() {
		return (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()=\"Validate\"]")));
		// driver.findElement(By.xpath("//button[text()=\"Validate\"]"));
	}

	public WebElement getMovieTitle() {
		return driver.findElement(By.name("title"));
	}

	public void setMovieTitle(String value) {
		WebElement inpt = getMovieTitle();
		inpt.clear();
		inpt.sendKeys(value);
	}

	public WebElement getTitleRequiredError() {
		return driver.findElements(By.xpath("//input[@name=\"title\"]/following-sibling::small")).get(0);
	}

	public WebElement getTitleLengthError() {
		return driver.findElements(By.xpath("//input[@name=\"title\"]/following-sibling::small")).get(1);
	}

	public void selectGenre(String value) {
		Select genre = new Select(driver.findElement(By.name("genre")));
		genre.selectByValue(value);
	}

	public WebElement getDirector() {
		return driver.findElement(By.name("director"));
	}

	public void setDirector(String value) {
		WebElement inpt = getDirector();
		inpt.clear();
		inpt.sendKeys(value);
	}

	public WebElement getDirectorRequiredError() {
		return driver.findElements(By.xpath("//input[@name=\"director\"]/following-sibling::small")).get(0);
	}

	public WebElement getDirectorLengthError() {
		return driver.findElements(By.xpath("//input[@name=\"director\"]/following-sibling::small")).get(1);
	}

	public WebElement getWriter() {
		return driver.findElement(By.name("writer"));
	}

	public void setWriter(String value) {
		WebElement inpt = getWriter();
		inpt.clear();
		inpt.sendKeys(value);
	}

	public WebElement getProducer() {
		return driver.findElement(By.name("producer"));
	}

	public void setProducer(String value) {
		WebElement inpt = getProducer();
		inpt.clear();
		inpt.sendKeys(value);
	}

	public WebElement getWebSite() {
		return driver.findElement(By.name("website"));
	}

	public void setWebSite(String value) {
		WebElement inpt = getWebSite();
		inpt.clear();
		inpt.sendKeys(value);
	}

	public WebElement getTrailer() {
		return driver.findElement(By.name("trailer"));
	}

	public void setTrailer(String value) {
		WebElement inpt = getTrailer();
		inpt.clear();
		inpt.sendKeys(value);
	}

	public void selectRating(String rating) {
		driver.findElement(By.name(rating)).click();
	}

}
