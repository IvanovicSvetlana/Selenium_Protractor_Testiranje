package primeri19;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;

public class Primerchormetestmoje {

	private WebDriver driver;
	private PrimerchromeMoje primerchrome;

	@BeforeMethod
	public void setUpSelenium() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		driver.navigate().to("http://www.calculator.net/");
		
		//instanciranje objekata
		primerchrome=new PrimerchromeMoje(driver);
	}

	@Test
	public void testCalculator() {
	    primerchrome.getNumberSeven().click();
		primerchrome.getPlusSign().click();
		primerchrome.getNumberSeven().click();;
		primerchrome.getEqualSign().click();
		primerchrome.getResult();
		Assert.assertEquals(primerchrome.getResult(), "14.");
		Assert.assertEquals(driver.getCurrentUrl(), "http://www.calculator.net/");

	}
	@Test
	public void testCalculator2() {
		
	}
	

	@AfterSuite
	public void afterSuite() {
		driver.close();
	}

}
