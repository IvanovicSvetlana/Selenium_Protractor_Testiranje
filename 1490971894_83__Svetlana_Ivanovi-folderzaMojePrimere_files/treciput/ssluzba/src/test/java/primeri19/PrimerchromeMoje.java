package primeri19;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PrimerchromeMoje {
WebDriver driver;

public PrimerchromeMoje(WebDriver driver) {
	super();
	this.driver = driver;
}
public WebElement getNumberSeven() {
	return driver.findElement(By.xpath("//SPAN[@onclick='r(7)'][text()='7']"));
}
public WebElement getPlusSign() {
	return driver.findElement(By.xpath("//SPAN[@class='sciop'][text()='+']"));
}
public WebElement getEqualSign() {
	return driver.findElement(By.xpath("//SPAN[@class='scieq'][text()='=']"));
}
public String getResult() {
	return driver.findElement(By.xpath("//DIV[@id='sciOutPut']")).getText();
}
//ovo su metode za Carbohydrate calculator
public WebElement getAgeTextArea() {
	return driver.findElement(By.xpath("//INPUT[@id='cage']"));
}
public void setAge(String godine) {
WebElement 	age=getAgeTextArea();
age.clear();
age.sendKeys(godine);
}
public WebElement getFemaleToBeClicked() {
	return driver.findElement(By.xpath("//INPUT[@id='csex1']"));
}
public WebElement getMaleToBeClicked() {
	return driver.findElement(By.xpath("//INPUT[@id='csex2']"));
}
public WebElement getHeightTextArea() {
	return driver.findElement(By.xpath("//INPUT[@id='cheightfeet']"));
}
public void setHeight(String visina) {
WebElement 	height=getHeightTextArea();
height.clear();
height.sendKeys(visina);
}
public WebElement getWeight() {
	return driver.findElement(By.id("cpound"));
}
public void setWeight(String tezina) {
WebElement 	weight=getWeight();
weight.clear();
weight.sendKeys(tezina);
}
////select/option[text()='Basal Metabolic Rate (BMR)']/../option[@value='1.2']
public WebElement getOption2() {
return driver.findElement(By.xpath("//select/option[text()='Basal Metabolic Rate (BMR)']/../option[@value='1.2']"));
}
/*<select id="cactivity" name="cactivity">
<option value="1">Basal Metabolic Rate (BMR)</option>
<option value="1.2">Sedentary - little or no exercise</option>
<option value="1.375" selected="">Lightly Active - exercise/sports 1-3 times/week</option>
<option value="1.55">Moderatetely Active - exercise/sports 3-5 times/week</option>
<option value="1.725">Very Active - hard exercise/sports 6-7 times/week</option>
<option value="1.9">Extra Active - very hard exercise/sports or physical job</option>
</select>*/
//<option value="1.9">Extra Active - very hard exercise/sports or physical job</option>
public WebElement getOptionExtraActive() {
	WebElement parent=driver.findElement(By.id("cactivity"));
	return parent.findElement(By.xpath("//option[text()='Extra Active - very hard exercise/sports or physical job']"));
}
////input[@name='printit']/input[@type='image' and @src='/img/calculate.png']
public WebElement getCalculateButton() {
	return (new WebDriverWait(driver, 15))
			.until(ExpectedConditions.elementToBeClickable(By.id("csex2")));
}
public WebElement getTitleResult() {
	return driver.findElement(By.xpath("//*[@id='content']/h2[1]"));
}
}