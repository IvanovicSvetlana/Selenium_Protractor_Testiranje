package primeri20;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Primer1 {
	public static void main(String[] args) throws Exception {
		WebDriver driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.navigate().to("http://www.calculator.net/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath(".//*[@id='menu']/div[3]/a")).click();
		driver.findElement(By.xpath(".//*[@id='menu']/div[4]/div[3]/a")).click();
		//check input presence
		System.out.println(Utils.isPresent(driver, By.id("cpar1")));
		driver.findElement(By.id("cpar1")).sendKeys("10");
		//check input presence
		System.out.println(Utils.isPresent(driver, By.id("cpar2")));
		driver.findElement(By.id("cpar2")).sendKeys("50");
		//check fake input presence 
		System.out.println(Utils.isPresent(driver, By.id("fakeeeee")));
		driver.findElement(By.xpath(".//*[@id='content']/table[1]/tbody/tr[2]/td/input[2]")).click();
		String result = driver.findElement(By.cssSelector("#content > p.verybigtext > font > b")).getText();
		// Print a Log In message to the screen
		Utils.takeSnapShot(driver, "test.png");
		System.out.println(" The Result is " + result);
		// Close the Browser.
		driver.close();
	}
}